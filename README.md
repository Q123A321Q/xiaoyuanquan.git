# 校园圈
#### 介绍
创新创业大赛校园项目（脱敏）
整合散列API
#### 软件架构

前端:使用Uniapp(Vue2、Vuex、Axios、Mock.js)、微信小程序、App（原生安卓）

后端:云原生、阿里云（云函数）

运维:Linux、宝塔面板、Nginx

#### 项目预览

![LOGO](./unpackage/res/icons/144x144.png)

![更新图片](https://images.gitee.com/uploads/images/2021/0524/155605_274f8b91_8038484.png "gx.png")
#### 首页
![首页](README_files/2.jpg)

#### 我的
![我的](README_files/5.jpg)

#### 安装教程
1. git clone https://gitee.com/Q123A321Q/xiaoyuanquan.git
2. 使用dir查看是否位于项目位置
3. cd xiaoyuanquan
4. npm i

#### 启动
1. npm run dev


#### 参与贡献
2751338876、UI、策划。。。。