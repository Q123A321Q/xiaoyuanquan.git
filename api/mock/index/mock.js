import Mock from "mockjs";
// 获取用户
Mock.mock("/api/user",{
    "code": 200,
    "data": {
        "name": "uni-app",
    }
})
Mock.mock("/api/login",{
    "code": 200,
    "list|10-30": [
        {
            "id|+1": 1,
        },
        {
            "username": "@cname",
        }
    ]
})

